import 'dart:async';

import 'package:get/state_manager.dart';
import 'package:faker/faker.dart';

class SocketClientController extends GetxController {

  RxString _message = "".obs;
  RxInt _counter = 0.obs;
  RxString get message => _message;

  Timer _timer,_timerCounter;
  final _faker = Faker();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _init();
  }

  _init() {

    ever(_counter,(_){
      print('_counter has been changed ${_counter.value}');
    });

    _timer = Timer.periodic(Duration(seconds: 5), (timer) {
      _message.value = _faker.lorem.sentence();
    });

    _timerCounter = Timer.periodic(Duration(seconds: 1), (timer) {
      _counter++;
    });
  }

  @override
  void onClose() {
    // TODO: implement onClose
    if(_timer != null)
    {
      _timer.cancel();
    }

    if(_timerCounter != null)
    {
      _timerCounter.cancel();
    }

    super.onClose();
  }
}
