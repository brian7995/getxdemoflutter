import 'package:get/state_manager.dart';
import 'package:get/route_manager.dart';

import 'package:getx_demo/api/users_api.dart';
import 'package:getx_demo/models/user.dart';
import 'package:getx_demo/pages/profile_page.dart';

class HomeController extends GetxController
{
  int _counter = 0;
  List<User> _users = [];
  bool _loading = true;

  bool get loading => _loading;
  int get counter => _counter;
  List<User> get users => _users;

  @override 
  void onInit()
  {
    super.onInit();
    print("same as initstate");
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    print('Ready!');
    loadUsers();
  }

  Future<void> loadUsers() async 
  {
    final data = await UsersApi.instance.getUsers(2);

    this._users = data;
    this._loading = false;
    update(['users']);
  }

  void increment()
  {
    this._counter++;
    update(['text'],_counter >= 5);
  }

  Future<void> showUserProfile(User user) async
  {
    final result = await Get.to<String>(ProfilePage(),arguments:user);

    if(result != null)
    {
      print('🤩 result : ${result} ');
    }else
    {
      print('result es nulo');
    }
  }

}