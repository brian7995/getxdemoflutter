
import 'dart:async';
import 'dart:io';

import 'package:get/get.dart';
import 'package:getx_demo/controllers/socket_client_controller.dart';
import 'package:getx_demo/models/pet.dart';

class ReactiveController extends GetxController
{
  RxInt counter = 1.obs;
  RxString currentDate = ''.obs;
  RxList<String> items = List<String>().obs;
  RxMap<String,dynamic> mapitems = Map<String,dynamic>().obs;

  Pet myPet = Pet(name: 'Lelo', age: 3);

  StreamSubscription<String> _subscription;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    final SocketClientController socketClientController = Get.find<SocketClientController>();
    _subscription  = socketClientController.message.listen((String data) {
      print('message:::::: $data');
    });
  }

   @override
  void onClose() {
    // TODO: implement onInit
    super.onClose();
  }

  

  void increment()
  {
    counter.value += 1;
  }

  void getDate()
  {
    currentDate.value = DateTime.now().toString();
  }

  void addItems()
  {
    items.add(DateTime.now().toString());
  }

  void addMapItems()
  {
    final String key = DateTime.now().toString();
    mapitems.add(key, key);
  }

  void removeItem(int index)
  {
    items.removeAt(index);
  }

  void removeMapItem(String key)
  {
    mapitems.remove(key);
  }

  void setPetAge(int age)
  {
    myPet.age = age;
  }
}