import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:getx_demo/controllers/reactive_controller.dart';
import 'package:getx_demo/controllers/socket_client_controller.dart';

class ReactivePage extends StatelessWidget {
  const ReactivePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final socketController = Get.find<SocketClientController>();
    return GetBuilder<ReactiveController>(
        init: ReactiveController(),
        builder: (_) {
          return Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Obx(() => Text('Age ${_.myPet.age}')),
                Obx(() => Text(socketController.message.value)),
                FlatButton(
                  child: Text('Por fin pude :) .Ese archivito me malograba '),
                  onPressed: (){
                    _.setPetAge(_.myPet.age + 1);   
                  }
                ),
              ],
            ),
            // body: Obx(
            //   () => ListView(
            //     children: _.mapitems.values.map(
            //       (e) => ListTile(
            //         title:Text(e),
            //         trailing: IconButton(icon: Icon(Icons.delete), onPressed: (){
            //           _.removeMapItem(e);
            //         }),
            //       )
            //     ).toList()
            //   )
            // ),
            // floatingActionButton: Row(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: [
            //     FloatingActionButton(
            //       heroTag: 'add',
            //       backgroundColor: Colors.green,
            //       child: Icon(Icons.add),
            //       onPressed: () {
            //         _.addMapItems();
            //     }),
            //     SizedBox(width:10.0),
            //     FloatingActionButton(
            //       heroTag: 'date',
            //       backgroundColor: Colors.redAccent,
            //       child: Icon(Icons.calendar_today),
            //       onPressed: () {
            //         _.getDate();
            //     }),
            //   ],
            // )
          );
        });
  }
}
